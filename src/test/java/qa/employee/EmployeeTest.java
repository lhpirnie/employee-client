package qa.employee;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmployeeTest {

	@Test
	public void testIncrementAge() {
		Employee e = new Employee("Fred", "Bloggs", 25);
		e.incAge();
		assertEquals(26, e.getAge());
	}
	
	@Test
	private void testNothing() {
		fail();
	}
	
	@Test
	public void testInitialAgeDefaultsTo21() {
		Employee e = new Employee("Fred", "Bloggs", 0);
		assertEquals(21, e.getAge());
	}

}
