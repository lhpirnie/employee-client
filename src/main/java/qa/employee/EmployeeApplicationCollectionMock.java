package qa.employee;
//a mock instead of the datebase
//stores data inside of an array list
//uses a collection instead of a db
//not persistent - shut down and it's gone

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EmployeeApplicationCollectionMock implements EmployeeApplication {
	
	private List<Employee> employees;
	private int nextId;

	
	public EmployeeApplicationCollectionMock() {
		employees = new ArrayList<>();
		nextId = 1;
	}

	@Override
	public String getEmployees() {
		StringBuilder sb = new StringBuilder();
		for (Employee e : employees) {
			sb.append(e.getId() + "\t");
			sb.append(e.getFirstname() + " ");
			sb.append(e.getLastname() + "\t");
			sb.append(e.getAge() + "\n");
		}
		return sb.toString();
	}

	@Override
	public boolean removeEmployee(int id) {
		Iterator<Employee> iter = employees.iterator();
		while (iter.hasNext()) {
			if (iter.next().getId()==id) {
				iter.remove();
				return true;
			}
		}
		return false;
	}

	@Override
	public void addEmployee(Employee emp) {
		emp.setId(nextId);
		nextId++;
		employees.add(emp);
	}

	@Override
	public void shutDown() {
	}
	

}
