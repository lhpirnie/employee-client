package qa.employee;

import java.util.Comparator;

public class Employee implements Comparable<Employee> {
	
	private String firstname;
	private String lastname;
	private int age;
	private int id;
	private static final int MIN_AGE = 21;

	private static int retirementAge = 68;
	public static final Comparator<Employee> LASTNAME_COMPARER = new EmployeeLastNameComparator();
	
	public Employee(String firstname, String lastname, int age) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		if (age < MIN_AGE) age = MIN_AGE;
		this.age = age;
	}
	
	public void incAge() {
		if (age < retirementAge) {
			age++;
		}
	}
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		return id;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Employee [firstname=" + firstname + ", lastname=" + lastname + ", id=" + id + "]";
	}
	@Override
	public int compareTo(Employee other) {
		return other.id - id;
	}
	public static int getRetirementAge() {
		return retirementAge;
	}

	public static void setRetirementAge(int retirementAge) {
		Employee.retirementAge = retirementAge;
	}
	
	private static class EmployeeLastNameComparator implements Comparator<Employee> {
		@Override
		public int compare(Employee e1, Employee e2) {
			return e1.lastname.compareTo(e2.lastname);
		}
	}

}
