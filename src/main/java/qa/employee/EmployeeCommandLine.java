package qa.employee;

public class EmployeeCommandLine {

	public static void main(String[] args) {
		if (args.length==3) {
			EmployeeApplication app = new EmployeeApplicationJDBC(args[0], args[1], args[2]);
			System.out.println("-----------Employees-----------");
			System.out.println(app.getEmployees());
			System.out.println("-------------------------------");
		} else {
			System.out.println("Incorrect parameters!  Usage: EmployeeFrame dburl username password OR EmployeeFrame mockclassname");
			System.exit(1);
		}

	}

}
