package qa.employee;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class EmployeeFrame extends JFrame {
	private JPanel titlePanel, westPanel, addEmpPanel, removePanel, addPanel, addButtonPanel, resultsPanel, outputPanel,
			removeEmpPanel, removeButtonPanel, showPanel, framePanel, removeNameTxtPanel;

	private JLabel titleLabel, addNewEmpLabel, addEmpAgeLabel, showLabel, statusLabel, removeIdLabel;

	private JTextField addEmpNameTxt, addEmpAgeTxt, removeIdTxt;

	private JButton removeButton, addButton;

	private JTextArea resultTxt, outputTxt;

	private Container theFrame;
	
	private static final int FONT_SIZE = 20;
	private static Font mainFont = new Font("Arial", Font.BOLD, FONT_SIZE);

	private EmployeeApplication employeeApp;
	
	public static void main(String[] args) {
		EmployeeApplication app = null;
		if (args.length==3) {
			app = new EmployeeApplicationJDBC(args[0], args[1], args[2]);
		} else if (args.length==1) {
			try {
				app = (EmployeeApplication) Class.forName(args[0]).newInstance();
			} catch (Exception e) {
				System.out.println("Incorrect parameters!  Usage: EmployeeFrame mockclassname (fully-qualified)");	
				e.printStackTrace();
				System.exit(1);
			} 
		} else {
			System.out.println("Incorrect parameters!  Usage: EmployeeFrame dburl username password OR EmployeeFrame mockclassname");
			System.exit(1);
		}
		final EmployeeFrame f = new EmployeeFrame(app);
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				f.shutDown();
				System.exit(0);
			}
		});
		f.setSize(1000, 750);
		f.setVisible(true);
	}


	public EmployeeFrame(EmployeeApplication app) {
		employeeApp = app;
		layoutComponents();
		registerListeners();
		showEmployees();
	}

	public void shutDown() {
		try {
			employeeApp.shutDown();
			this.dispose();
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
	}

	private void applyFont(Component... comps) {
		for (Component c : comps) {
			c.setFont(mainFont);	
		}
	}
	
	public void layoutComponents() {
		theFrame = getContentPane();
		theFrame.setLayout(new BorderLayout(10, 10));

		theFrame.add(framePanel = new JPanel(new BorderLayout(10, 10)));
		Border frameBorder = BorderFactory.createLineBorder(Color.blue, 5);
		framePanel.setBorder(frameBorder);

		// North sector
		titlePanel = new JPanel();
		titleLabel = new JLabel("Employee Application");
		titleLabel.setFont(new java.awt.Font("Serif", 0, FONT_SIZE + 4));
		titlePanel.add(titleLabel, null);

		framePanel.add(titlePanel, BorderLayout.NORTH);

		// West sector
		westPanel = new JPanel(new GridLayout(2, 1));

		// adding an employee
		addPanel = new JPanel(new BorderLayout());
		Border eb = BorderFactory.createEtchedBorder();
		TitledBorder addBorder = BorderFactory.createTitledBorder(eb, "Add Employee");
		addPanel.setBorder(addBorder);

		addEmpPanel = new JPanel(new GridLayout(4, 1));
		addEmpPanel.add(addNewEmpLabel = new JLabel("Enter name:"));
		addEmpPanel.add(addEmpNameTxt = new JTextField());
		addEmpPanel.add(addEmpAgeLabel = new JLabel("Enter age:"));
		addEmpPanel.add(addEmpAgeTxt = new JTextField());
		
		applyFont(addEmpAgeLabel, addNewEmpLabel, addEmpNameTxt, addEmpAgeTxt);
		
		addPanel.add(addEmpPanel, BorderLayout.CENTER);

		addButtonPanel = new JPanel();
		addButtonPanel.add(addButton = new JButton("Add"));
		addPanel.add(addButtonPanel, BorderLayout.SOUTH);

		westPanel.add(addPanel);

		applyFont(addButton);
		// removing a member
		removePanel = new JPanel(new BorderLayout());
		eb = BorderFactory.createEtchedBorder();
		TitledBorder removeBorder = BorderFactory.createTitledBorder(eb, "Remove Employee");
		removePanel.setBorder(removeBorder);

		removeEmpPanel = new JPanel(new GridLayout(2, 1));
		removeEmpPanel.add(removeIdLabel = new JLabel("Enter ID:"));

		// local class to allow you to inset a component in a Panel
		class TextPanel extends JPanel {
			public TextPanel(LayoutManager lm) {
				super(lm);
			}

			public Insets getInsets() {
				return new Insets(5, 0, 5, 0);
			}
		}
		removeNameTxtPanel = new TextPanel(new BorderLayout());
		removeNameTxtPanel.add(removeIdTxt = new JTextField(), BorderLayout.CENTER);
		removeEmpPanel.add(removeNameTxtPanel);
		removePanel.add(removeEmpPanel, BorderLayout.CENTER);
		
		removeButtonPanel = new JPanel();
		removeButtonPanel.add(removeButton = new JButton("remove member"));
		removePanel.add(removeButtonPanel, BorderLayout.SOUTH);

		applyFont(removeIdTxt, removeIdLabel, removeButton);
		
		westPanel.add(removePanel);

		framePanel.add(westPanel, BorderLayout.WEST);

		// Center sector
		resultsPanel = new JPanel(new BorderLayout());

		showPanel = new JPanel();
		showPanel.add(showLabel = new JLabel("All the current Employees"));
		resultsPanel.add(showPanel, BorderLayout.NORTH);

		resultsPanel.add(resultTxt = new JTextArea(), BorderLayout.CENTER);

		applyFont(resultTxt, showLabel);

		framePanel.add(resultsPanel, BorderLayout.CENTER);

		// South sector
		outputPanel = new JPanel();
		eb = BorderFactory.createEtchedBorder();
		TitledBorder outputBorder = BorderFactory.createTitledBorder(eb, "Status");
		outputPanel.setBorder(outputBorder);

		outputPanel.add(outputTxt = new JTextArea(2, 40));
		outputTxt.setFont(mainFont);

		framePanel.add(outputPanel, BorderLayout.SOUTH);
		applyFont(outputTxt);
		
		addEmpNameTxt.grabFocus();
	}

	public void registerListeners() {
		// anonymous class to respond to an "add employee" request
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				String name = addEmpNameTxt.getText();
				String strAge = addEmpAgeTxt.getText();
				if (isValidEntry(name, strAge)) {
					int age = 0;
					try {
						age = Integer.parseInt(strAge);
						employeeApp.addEmployee(createEmployee(name, age));
						outputTxt.setText("Employee Added");
						addEmpNameTxt.setText("");
						addEmpAgeTxt.setText("");
						addEmpNameTxt.grabFocus();
						showEmployees();
					} catch (NumberFormatException ne) {
						outputTxt.setText("Age must be a number");
						addEmpAgeTxt.grabFocus();
					}
				}
			}
		});

		// anonymous class to respond to a "remove employee" request
		removeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				String idTxt = removeIdTxt.getText();
				try {
					int id = Integer.parseInt(idTxt);
					if (employeeApp.removeEmployee(id)) {
						outputTxt.setText("Employee removed");
						removeIdTxt.setText("");
						removeIdTxt.grabFocus();
						showEmployees();
					} else {
						outputTxt.setText("No such Employee");
					}
				} catch (NumberFormatException ne) {
					removeIdTxt.grabFocus();
				}
			}
		});
	}

	// helper method to format and display the current employees
	private void showEmployees() {
		resultTxt.setFont(new Font("Courier", Font.BOLD, 18));
		resultTxt.setText("IDNUM\tNAME\t\tAGE\n");
		resultTxt.setFont(new Font("Courier", Font.PLAIN, 15));
		String mems = employeeApp.getEmployees();
		resultTxt.append(mems);
	}

	// helper method to validate a name and age
	private boolean isValidEntry(String name, String age) {
		boolean result = false;
		if (isValidEntry(name)) {
			if (age.equals("")) {
				outputTxt.setText("Must enter an age");
				addEmpAgeTxt.grabFocus();
			} else {
				result = true;
			}
		} else {
			if (age.equals("")) {
				outputTxt.append(" and age");
			}
		}
		return result;
	}

	// overloaded helper to validate a name
	private boolean isValidEntry(String name) {
		if (!name.matches("[A-Z]{1}[a-z]{1,} [A-Z]{1}[a-z]{1,}")) {
			outputTxt.setText("Must enter a name");
			addEmpNameTxt.grabFocus();
			return false;
		} else {
			return true;
		}
	}
	
	private Employee createEmployee(String fullName, int age) {
		String[] parts = fullName.split(" ");
		return new Employee(parts[0], parts[1], age);
	}

	public Insets getInsets() {
		return new Insets(10, 10, 10, 10);
	}

}