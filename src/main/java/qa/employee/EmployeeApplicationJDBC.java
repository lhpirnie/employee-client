package qa.employee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class EmployeeApplicationJDBC implements EmployeeApplication {
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private Connection con;

	public EmployeeApplicationJDBC(String dbUrl, String username, String password) {
		// to do:
		// add code to create (not declare!) a connection to the datasource
		// with URL: jdbc:mysql://localhost/employees
		// and store the reference in the instance variable provided
		// you will need to use the userName and password variables

		try {
			con = DriverManager.getConnection(dbUrl, username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// helper method which finds out the highest current idnum in the table
	// so as to avoid clashes when adding new employees
	private int getCurrentIDNum() {
		int result = 0;
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("SELECT idNum FROM employees");
			while (rs.next()) {
				int idNum = rs.getInt("idNum");
				if (idNum > result) {
					result = idNum;
				}
			}
		} catch (SQLException se) {
			System.out.println(se);
		}
		return result;
	}

	// returns a formatted String representation of the current employees
	/* (non-Javadoc)
	 * @see qa.employee.EmployeeApplication#getEmployees()
	 */
	@Override
	public String getEmployees() {
		StringBuilder sb = new StringBuilder(200);

		// to do:
		// ask the Connection for a Statement and use it to retrieve
		// all the data from the employees table. Then process the data
		// and format your results by appending them to sb.

		try {
			Statement stmt = con.createStatement();
			System.out.println(stmt);
			ResultSet res = stmt.executeQuery("SELECT idNum, firstname, lastname, age FROM employees");
			System.out.println(res);
			while (res.next()) {
				sb.append(res.getInt(1) + "\t");
				sb.append(res.getString(2) + " ");
				sb.append(res.getString(3) + "\t");
				sb.append(res.getInt(4) + "\n");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

	// removes the specified employee
	/* (non-Javadoc)
	 * @see qa.employee.EmployeeApplication#removeEmployee(java.lang.String)
	 */
	@Override
	public boolean removeEmployee(int id) {
		boolean result = false;

		// to do:
		// remove any entries from the table which have the id
		// which has been passed in as a parameter. If the row was successfully
		// removed then set result to be true, otherwise leave it as false
		try {
			Statement stmt = con.createStatement();
			int res = stmt.executeUpdate("DELETE FROM employees WHERE idNum = '" + id + "'");
			if (res==1) result = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}

	// creates a new Employee and adds it to the collection
	/* (non-Javadoc)
	 * @see qa.employee.EmployeeApplication#addEmployee(java.lang.String, int)
	 */
	@Override
	public void addEmployee(Employee emp) {
		// to do:
		// use the parameters and the instance variable currentIDNum
		// to add a new row to the table. You will need to create a Statement
		// first and then use the information you have to build up an sql string
		// for the statement to work with
		int id = getCurrentIDNum() + 1;
		
		try {
			Statement stmt = con.createStatement();
			stmt.executeUpdate("INSERT INTO employees VALUES (" + id + ",'" + emp.getFirstname() + "','" + emp.getLastname() + "'," + emp.getAge() + ")");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/* (non-Javadoc)
	 * @see qa.employee.EmployeeApplication#shutDown()
	 */
	@Override
	public void shutDown() {
		try {
			con.close();
		} catch (Exception e) {
		}
	}

}