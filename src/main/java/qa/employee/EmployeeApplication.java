package qa.employee;

public interface EmployeeApplication {

	// returns a formatted String representation of the current employees
	String getEmployees();

	// removes the specified employee
	boolean removeEmployee(int id);

	// creates a new Employee and adds it to the collection
	void addEmployee(Employee emp);

	void shutDown();

}